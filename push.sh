#!/usr/bin/env zsh
set -e
cd "${0%/*}" || exit

VERSION="0.2.0"

docker build -t "registry.gitlab.com/david-polak/img-postgresql-pgvecto-rs:${VERSION}" .
docker push "registry.gitlab.com/david-polak/img-postgresql-pgvecto-rs:${VERSION}"

ARG BITNAMI_POSTGRES_BASE_VERSION="16.1.0"
ARG PGVECTO_RS_BASE_VERSION="v0.2.0"

FROM tensorchord/pgvecto-rs-binary:pg16-${PGVECTO_RS_BASE_VERSION}-amd64 as binary
FROM bitnami/postgresql:${BITNAMI_POSTGRES_BASE_VERSION} as build

USER root
COPY --from=binary /pgvecto-rs-binary-release.deb /tmp/vectors.deb
RUN apt-get install -y /tmp/vectors.deb \
 && rm -f /tmp/vectors.deb \
 && mv /usr/share/postgresql/16/extension/* /opt/bitnami/postgresql/share/extension/ \
 && mv /usr/lib/postgresql/16/lib/* /opt/bitnami/postgresql/lib/ \
 && rm -r /usr/share/postgresql

USER postgres
